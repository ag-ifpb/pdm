package ifpb.receiver;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main3Activity extends AppCompatActivity {
    Button btnAsyncTask;
    Button btnColor;
    AsyncTask<Void, Void, Void> asyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        //
        btnAsyncTask = (Button) findViewById(R.id.btnasynctask);
        btnColor = (Button) findViewById(R.id.btncolor);
        //
        btnColor.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (btnColor.getText().equals("Mudar Cor")){
                    btnColor.setText("Cor alterada");
                } else {
                    btnColor.setText("Mudar Cor");
                }
            }
        });
        //
        btnAsyncTask.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                asyncTask.execute();
            }
        });
        //
        asyncTask = new AsyncTask<Void, Void, Void>() {
            private String name;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                name = btnAsyncTask.getText().toString();
                btnAsyncTask.setText("Está executando");
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                btnAsyncTask.setText(name);
            }
        };

    }
}
