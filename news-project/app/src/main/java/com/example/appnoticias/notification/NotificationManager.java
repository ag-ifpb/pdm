package com.example.appnoticias.notification;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.example.appnoticias.screen.TelaCadastro;

public class NotificationManager {
    private final Context ctx;
    private final String channelID = "mychannel";

    public NotificationManager(Context ctx){
        this.ctx = ctx;
    }

    public void sendNotification(int smallIcon, String title, String content, Class<?> actClass){
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(ctx, actClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0, intent, 0);
        //create notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx, channelID)
                .setSmallIcon(smallIcon)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        //notify
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(ctx);
        notificationManager.notify(0, builder.build());
    }
}