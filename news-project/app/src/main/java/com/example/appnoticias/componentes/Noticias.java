package com.example.appnoticias.componentes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Noticias extends LinearLayout {

    private TextView titulo;
    private TextView descricao;
    private TextView data;
    private LinearLayout layoutInformacoes;


    public Noticias(Context ctx, String titulo, String descricao, String data) {
        super(ctx);
        this.init(ctx, titulo, descricao, data);
        this.noticeWithoutLayoutImage(ctx);
    }

    private void init(Context ctx, String titulo, String descricao, String data) {
        setOrientation(VERTICAL);

        this.titulo = new TextView(ctx);
        this.titulo.setText(titulo);
        this.titulo.setTextSize(12);
        this.titulo.setTextColor(Color.BLACK);
        this.titulo.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        this.titulo.setWidth(10);
        this.titulo.setPadding(0,20,0,0);

        this.descricao = new TextView(ctx);
        this.descricao.setText(descricao);
        this.descricao.setTextSize(10);
        this.descricao.setTextColor(Color.BLACK);
        this.descricao.setPadding(20,10,20,0);
        this.descricao.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        this.descricao.setWidth(10);
        this.descricao.setPadding(0,20,0,0);


        this.data = new TextView(ctx);
        this.data.setText(data);
        this.data.setTextSize(10);
        this.data.setTextColor(Color.BLACK);
        this.data.setPadding(0,10,0,0);
        this.data.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        this.data.setWidth(10);
        this.data.setPadding(0,20,0,0);

    }

    private void noticeWithoutLayoutImage(Context context){
        //
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        //
        this.layoutInformacoes = new LinearLayout(context);
        this.layoutInformacoes.setLayoutParams(p);
        this.layoutInformacoes.setOrientation(VERTICAL);
        this.layoutInformacoes.addView(this.titulo);
        this.layoutInformacoes.addView(this.descricao);
        this.layoutInformacoes.addView(this.data);
        addView(this.layoutInformacoes);
    }

}
