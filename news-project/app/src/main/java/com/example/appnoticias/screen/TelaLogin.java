package com.example.appnoticias.screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.appnoticias.componentes.Botao;
import com.example.appnoticias.componentes.Input;
import com.example.appnoticias.notification.NotificationManager;
import com.example.appnoticias.persistence.configuration.AccessDTO;
import com.example.appnoticias.persistence.configuration.AccessManager;
import com.example.appnoticias.persistence.database.UsuarioDao;
import com.example.appnoticias.Model.Usuario;
import com.example.appnoticias.R;
import com.example.appnoticias.rss.GetRss;

import java.util.Date;

public class TelaLogin extends AppCompatActivity {

    private void gotoNewsList(){
        Intent mudaIntent = new Intent(getApplicationContext(), GetRss.class);
        startActivity((mudaIntent));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TITULO DA PAGINA
        TextView titulo = new TextView(this);
        titulo.setPadding(0,80,0,0);
        titulo.setText("LOGIN\n\n");
        titulo.setTextSize(20f);
        titulo.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        //LAYOUT DOS BOTOES
        LinearLayout layoutBotoes= new LinearLayout(this);
        layoutBotoes.setLayoutParams(new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        layoutBotoes.setOrientation(LinearLayout.VERTICAL);
        layoutBotoes.setPadding(9,9,9,9);
        layoutBotoes.setGravity(Gravity.CENTER_HORIZONTAL);

        //LAYOUT DOS INPUTS
        LinearLayout layoutInputs= new LinearLayout(this);
        layoutInputs.setLayoutParams(new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        layoutInputs.setOrientation(LinearLayout.VERTICAL);
        layoutInputs.setPadding(9,5,9,9);
        layoutInputs.setGravity(Gravity.CENTER_HORIZONTAL);

        //LAYOUT DOS LAYOUTS (MAIN)
        LinearLayout linearLayout= new LinearLayout(this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(9,9,9,9);
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        setContentView(linearLayout);

        // LAYOUT IMAGE
        LinearLayout imagemL= new LinearLayout(this);
        imagemL.setLayoutParams(new LinearLayout.LayoutParams( 300,400));
        imagemL.setOrientation(LinearLayout.VERTICAL);
        imagemL.setPadding(0,10,0,0);
        imagemL.setGravity(Gravity.CENTER_HORIZONTAL);

        ImageView imagem = new ImageView(this);
        imagem.setImageResource(R.drawable.logo);
        imagemL.addView(imagem);

        getWindow().getDecorView().setBackgroundColor(Color.WHITE);

        //MUDANDO TITULO DA PAGINA
        setTitle("Login");

        //INPUTS DA PAGINA
        final Input email = new Input(this,"Email",
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, 500);
        layoutInputs.addView(email);

        final Input senha = new Input(this,"Senha",
                InputType.TYPE_TEXT_VARIATION_PASSWORD, 500);
        layoutInputs.addView(senha);


        //ATRIBUINDO OS BOTOES
        Botao botaoEntrar = new Botao(this, "Entrar",
                Color.rgb(255,69,0));
        botaoEntrar.setColorTextButton();
        botaoEntrar.setOnClickAction(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UsuarioDao usuarioDao = new UsuarioDao(TelaLogin.this);
                Usuario usuario = usuarioDao.buscarUsuario(email.getValue());
                usuarioDao.usuarioAutenticar(usuario.getEmail(),usuario.getSenha());
                if (email.getValue().equals(usuario.getEmail())&& senha.getValue().equals(usuario.getSenha())){
                    //create o access data
                    AccessDTO dto = new AccessDTO();
                    dto.setToken(String.valueOf(usuario.hashCode()));
                    dto.setName(usuario.getNome());
                    dto.setCode(usuario.getCodigo());
                    //save access data
                    AccessManager tm = new AccessManager(TelaLogin.this);
                    tm.store(dto);
                    //forward to news list
                    gotoNewsList();
                }else {
                    Toast.makeText(TelaLogin.this,"Email ou senha inválidos!",Toast.LENGTH_LONG).show();
                }

            }
        });
        layoutBotoes.addView(botaoEntrar);

        Botao botaoCriarConta = new Botao(this, "Criar Contar",
                Color.rgb(255,69,0));
        botaoCriarConta.setColorTextButton();
        botaoCriarConta.setOnClickAction(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("RAULT","FOI PRO CADASTRO");
                Intent mudarTelaCadastro = new Intent(getApplicationContext(), TelaCadastro.class);
                startActivity(mudarTelaCadastro);
            }
        });
        layoutBotoes.addView(botaoCriarConta);

        Button agbtn = new Button(this);
        agbtn.setOnClickListener((View v)->{
            NotificationManager nm = new NotificationManager(this);
            nm.sendNotification(R.drawable.logo, "Notificação de teste", "Minha notificação", TelaCadastro.class);
        });
        agbtn.setText("Enviar Notificação");
        layoutBotoes.addView(agbtn);



        //ATRIBUINDO TODAS AS VIEWS TANTO DOS INPUTS QUANTO DOS BOTOES
        linearLayout.addView(imagemL);
        linearLayout.addView(layoutInputs);
        linearLayout.addView(layoutBotoes);
    }

    @Override
    protected void onStart() {
        super.onStart();
        AccessManager tm = new AccessManager(this);
        // verificar se existe um token
        // caso exista um token encaminhar para
        // Lista de Notícias
        if (tm.checkToken()){
            gotoNewsList();
        }
    }
}
