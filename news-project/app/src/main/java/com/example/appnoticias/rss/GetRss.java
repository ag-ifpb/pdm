package com.example.appnoticias.rss;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.os.Bundle;
import com.example.appnoticias.componentes.GetNoticeAdapter;
import com.example.appnoticias.Model.Noticia;
import com.example.appnoticias.screen.NoticiaComple;
import com.example.appnoticias.screen.SideBar;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetRss extends SideBar {

    private ArrayList<String> links = new ArrayList<>();
    private List<Noticia> noticias = new ArrayList<>();

    private LinearLayout layoutPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layoutPrincipal = new LinearLayout(this);
        layoutPrincipal.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        );
        layoutPrincipal.setGravity(Gravity.CENTER_HORIZONTAL);
        layoutPrincipal.setPadding(0, 0, 0, 0);
        layoutPrincipal.setOrientation(LinearLayout.VERTICAL);
        setDynamicContent(layoutPrincipal);

        ListView listView = new ListView(this);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Noticia ns = noticias.get(position);
                Intent intent = new Intent(getApplicationContext(), NoticiaComple.class);
                intent.putExtra("noticiaUnica",ns);
                startActivity(intent);
            }
        });
        layoutPrincipal.addView(listView);
        //instanciar async task
        new ProcessaChamadaEmBackground(
                this, noticias, listView
        ).execute();

    }


    private InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }


    public static class ProcessaChamadaEmBackground extends AsyncTask<Integer, Void, String> {
        private final ProgressDialog progressDialog;
        private final List<Noticia> noticias;
        private final ListView listView;
        private final Context ctx;


        ProcessaChamadaEmBackground(Context ctx, List<Noticia> noticias, ListView listView){
            this.ctx = ctx;
            this.progressDialog = new ProgressDialog(this.ctx);
            this.noticias = noticias;
            this.listView = listView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Buscando Noticias ... aguarde !");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String html = null;
            try {
                //URL url = new URL("http://uirauna.net/feed/");
                URL url = new URL("http://radarpb.com.br/feed/");

                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(false);

                XmlPullParser xmlPullParser = factory.newPullParser();
                xmlPullParser.setInput(url.openConnection().getInputStream(), "utf_8");

                boolean insideItem = false;
                int eventType = xmlPullParser.getEventType();

                Noticia noticia = null;
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        if (xmlPullParser.getName().equalsIgnoreCase("item")) {
                            noticia = new Noticia();
                            insideItem = true;

                        } else if (xmlPullParser.getName().equalsIgnoreCase("title") && (noticia != null)) {
                            if (insideItem) {
                                noticia.setTitulo(xmlPullParser.nextText());
                            }
                        } else if (xmlPullParser.getName().equalsIgnoreCase("pubdate") && (noticia != null)) {
                            if (insideItem) {
                                noticia.setData(xmlPullParser.nextText());
                            }
                        } else if (xmlPullParser.getName().equalsIgnoreCase("link") && (noticia != null)) {
                            if (insideItem) {
                                noticia.setLink(xmlPullParser.nextText());
                            }
                        } else if (xmlPullParser.getName().equalsIgnoreCase("content:encoded") && (noticia != null)) {
                            if (insideItem) {
                                String htmlConteudo = xmlPullParser.nextText();
                                noticia.setConteudo(Html.fromHtml(htmlConteudo).toString());
                            }
                        } else if (xmlPullParser.getName().equalsIgnoreCase("description") && (noticia != null)) {
                            if (insideItem) {
                                noticia.setDescricao(Html.fromHtml(xmlPullParser.nextText()).toString());
                            }
                        }
                    } else if (eventType == XmlPullParser.END_TAG && xmlPullParser.getName().equalsIgnoreCase("item") && (noticia != null)) {
                        insideItem = false;
                        noticias.add(noticia);
                    }
                    eventType = xmlPullParser.next();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return html;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            GetNoticeAdapter notice = new GetNoticeAdapter(ctx,noticias);
            listView.setAdapter(notice);
            progressDialog.dismiss();

        }
    }
}
