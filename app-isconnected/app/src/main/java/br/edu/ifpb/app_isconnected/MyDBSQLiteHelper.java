package br.edu.ifpb.app_isconnected;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class MyDBSQLiteHelper extends SQLiteOpenHelper {
    private static String NAME = "mydatabase";
    private static int VERSION = 2;

    public MyDBSQLiteHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE person(_id integer primary key autoincrement, " +
                "name text not null, cpf text not null);");
        Log.d("AGDebug", "onCreate executado");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //criar novas tabelas
        //alterar tabelas anteriores
        //excluir dados/tabelas
        Log.d("AGDebug", "onUpgrade executado");
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        Log.d("AGDebug", "onDowngrade executado");
    }
}
