package ag.ag_webviewtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        WebView wv = (WebView) findViewById(R.id.webview);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient());
        wv.setWebChromeClient(new WebChromeClient());
        //
        String html = "" +
                "<!doctype html>" +
                "<html>" +          //obrigatório
                "   <body>" +
                "       <h1 style='color:#ccc'>Hello World</h1>" +
                "   </body>" +
                "</html>";
        //
        //wv.loadData(html, "text/html", "UTF-8");
        //wv.loadUrl("file:///android_asset/index.html");
        wv.loadUrl("http://myserver.com");
        wv.addJavascriptInterface(new JSInterface(this), "navigator");
    }

}
