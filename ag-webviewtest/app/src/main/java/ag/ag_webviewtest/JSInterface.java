package ag.ag_webviewtest;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.widget.Toast;

/**
 * Created by arigarcia on 9/22/16.
 */
public class JSInterface {
    private Context context;

    public JSInterface(Context ctx){
        context = ctx;
    }

    @JavascriptInterface
    public void showMessage(String text){
        Toast.makeText(context, "Oi Fernandinha!!! " + text, Toast.LENGTH_LONG)
                .show();
    }

    @JavascriptInterface
    public int sum(int x, int y){
        return x + y;
    }

}
