package ifpb.ifpbapp;

/**
 * Created by arigarcia on 7/19/17.
 */

public class Atualizador {
    private static final Atualizador ourInstance = new Atualizador();

    private MainActivity instancia;

    private Atualizador() {
    }

    public static Atualizador getInstance() {
        return ourInstance;
    }

    public static void setMainActivityInstance(MainActivity inst){
        getInstance().instancia = inst;
    }

    public static MainActivity getMainActivityInstance(){
        return getInstance().instancia;
    }




}
