package ifpb.ifpbapp;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {
    private RadioButton atividade1;
    private RadioButton atividade2;
    private Button iniciarServico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        Atualizador.setMainActivityInstance(this);
        //
        atividade1 = (RadioButton) findViewById(R.id.radioButton);
        atividade2 = (RadioButton) findViewById(R.id.radioButton2);
        iniciarServico = (Button) findViewById(R.id.button);
        iniciarServico.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this.getApplicationContext(),
                        MainService.class);
                MainActivity.this.startService(intent);
            }
        });
        //

    }

    public void marcarComoAtividade1(){
        atividade1.setChecked(true);
        atividade2.setChecked(false);
    }

    public void marcarComoAtividade2(){
        atividade1.setChecked(false);
        atividade2.setChecked(true);
    }
}
