package ifpb.ifpbapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //UIThread 1
        Toast.makeText(getBaseContext(), "Atualizar o item 1", Toast.LENGTH_LONG).show();
        Atualizador.getMainActivityInstance().marcarComoAtividade1();
        //
        Executor executor =  Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                long count = 0;
                while(true){
                    if ((count++) > 3000000000L){
                        break;
                    }
                }
            }
        });
        //
        Toast.makeText(getBaseContext(), "Atualizar o item 2", Toast.LENGTH_LONG).show();
        Atualizador.getMainActivityInstance().marcarComoAtividade2();
        //
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
