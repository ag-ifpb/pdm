package ifpb.ag.ag_firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    public void seeToken(View view){
        if (FirebaseInstanceId.getInstance() != null){
            Log.d("AGDebug", FirebaseInstanceId.getInstance().getToken());
        }
    }
}
