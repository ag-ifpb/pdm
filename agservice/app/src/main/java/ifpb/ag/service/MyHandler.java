package ifpb.ag.service;

import android.os.Handler;
import android.os.Looper;

class MyHandler extends Handler {
    private static MyHandler ourInstance = null;

    private MyHandler(Looper mainLooper, Handler.Callback callback) {
        super(mainLooper, callback);
    }

    static void init(Looper mainLooper, Handler.Callback callback){
        ourInstance = new MyHandler(mainLooper, callback);
    }

    static MyHandler getInstance() {
        return ourInstance;
    }

}
