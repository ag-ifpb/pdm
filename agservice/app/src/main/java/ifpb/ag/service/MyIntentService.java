package ifpb.ag.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class MyIntentService extends IntentService {
    private final static String TAG = "AGDEBUG";

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //log
        Log.d(TAG, "Iniciando o comando");
        //é assim!!
        Message msg = Message.obtain();
        msg.obj = "Hello World";
        Handler handler = MyHandler.getInstance();
        handler.sendMessageDelayed(msg, 10000);
        Log.d(TAG, "contagem");
        //MainActivity.INSTANCE.button.setText("Hello World");
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //log
        Log.d(TAG, "Passou o comando");
    }


}
