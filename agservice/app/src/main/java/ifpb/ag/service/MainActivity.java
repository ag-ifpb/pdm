package ifpb.ag.service;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "AGDEBUG";
    public Button button;
    public static MainActivity INSTANCE = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        button = new Button(this);
        button.setText("Executar o serviço");
        button.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                //
                INSTANCE = MainActivity.this;
                //
                Log.d(TAG, "Solicitando o serviço");
                Intent intservice = new Intent(MainActivity.this, MyIntentService.class);
                MainActivity.this.startService(intservice);
            }
        });
        //
        MyHandler.init(getMainLooper(), new Handler.Callback(){
            @Override
            public boolean handleMessage(Message message) {
                String text = (String) message.obj;
                button.setText(text);
                return true;
            }
        });
        //
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        LinearLayout layout = new LinearLayout(this);
        layout.setGravity(Gravity.CENTER);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(button);
        //
        setContentView(layout);
    }

}
