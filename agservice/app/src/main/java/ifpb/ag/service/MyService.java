package ifpb.ag.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

public class MyService extends Service {
    private final static String TAG = "AGDEBUG";

    public MyService() {}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //log
        Log.d(TAG, "Iniciando o comando");
        //é assim!!
        Message msg = Message.obtain();
        msg.obj = "Hello World";
        Handler handler = MyHandler.getInstance();
        handler.sendMessageDelayed(msg, 10000);//bloqueante??
        Log.d(TAG, "contagem");
        //
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //log
        Log.d(TAG, "Passou o comando");
        //
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //log
        Log.d(TAG, "Tentando vincular o serviço");
        //vinculação, caso seja um serviço vinculado
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
