package ifpb.ag.sidebarprogramatically.sidebar;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;


public class Sidebar extends VerticalContainer {


    public Sidebar(Context context) {
        super(context);
        //estiling
        setBackgroundColor(Color.BLUE);
        setPadding(10, 20, 10, 10);

    }

    public void addItem(Drawable icon, String text){
        SidebarItem item = new SidebarItem(getContext());
        item.setImageSource(icon);
        item.setText(text);
        addView(item);
    }

    public void show(){
        setVisibility(View.VISIBLE);
    }

    public void hide(){
        setVisibility(View.GONE);
    }

}
