package ifpb.ag.sidebarprogramatically.sidebar;

import android.content.Context;
import android.widget.LinearLayout;

public class VerticalContainer extends LinearLayout  {

    public VerticalContainer(Context context) {
        super(context);
        setLayoutParams(
                new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.MATCH_PARENT
        ));
        setOrientation(LinearLayout.VERTICAL);
    }

}
