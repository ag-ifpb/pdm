package ifpb.ag.sidebarprogramatically.sidebar;

import android.content.Context;
import android.widget.LinearLayout;

public class HorizontalContainer extends LinearLayout  {

    public HorizontalContainer(Context context) {
        super(context);
        setLayoutParams(
                new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        ));
        setOrientation(LinearLayout.HORIZONTAL);
    }

}
