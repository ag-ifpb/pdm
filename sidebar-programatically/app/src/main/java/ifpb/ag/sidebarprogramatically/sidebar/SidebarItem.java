package ifpb.ag.sidebarprogramatically.sidebar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

public class SidebarItem extends HorizontalContainer {
    private final ImageView imageView;
    private final TextView textView;

    public SidebarItem(Context ctx){
        super(ctx);
        //icon
        imageView = new ImageView(ctx);
        addView(imageView);
        //text
        textView = new TextView(ctx);
        addView(textView);
    }

    public void setImageSource(Drawable drawable){
        imageView.setImageDrawable(drawable);
    }

    public void setText(String value){
        textView.setText(value);
    }

}
