package ifpb.ag.sidebarprogramatically.sidebar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


public class SidebarAnimated extends Sidebar {
    private boolean toVisible;
    private ValueAnimator valueAnimator;


    public SidebarAnimated(Context context) {
        super(context);
        //estiling
        setBackgroundColor(Color.BLUE);
        setPadding(10, 20, 10, 10);
        //setVisibility(GONE);
        //
//        animate().setDuration(200).setListener(new AnimatorListenerAdapter(){
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                if (!toVisible) {
//                    setVisibility(View.GONE);
//                }
//            }
//        });
        //
        valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(1000);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                SidebarAnimated.this.getLayoutParams().width = (Integer) valueAnimator.getAnimatedValue("width");
                SidebarAnimated.this.setScaleX ((Float) valueAnimator.getAnimatedValue("scaleX"));
                SidebarAnimated.this.requestLayout();
            }

        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!toVisible) {
                    setVisibility(View.GONE);
                }
            }
        });

    }

    public void addItem(Drawable icon, String text){
        SidebarItem item = new SidebarItem(getContext());
        item.setImageSource(icon);
        item.setText(text);
        addView(item);
    }

    public void show(){
        toVisible = true;
        setVisibility(View.VISIBLE);
        //animate().translationX(0).scaleX(1).start();
        valueAnimator.setValues(PropertyValuesHolder.ofInt("width", 0, 200),
                PropertyValuesHolder.ofFloat("scaleX",  0, 1f));
        valueAnimator.start();
    }

    public void hide(){
        toVisible = false;
        //animate().translationX(-150).scaleX(0).start();
        valueAnimator.setValues(PropertyValuesHolder.ofInt("width", 200, 0),
                PropertyValuesHolder.ofFloat("scaleX",   1f, 0));
        valueAnimator.start();
    }

}
