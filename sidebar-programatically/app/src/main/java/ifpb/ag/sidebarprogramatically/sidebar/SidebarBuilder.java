package ifpb.ag.sidebarprogramatically.sidebar;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class SidebarBuilder {
    private Sidebar sidebar;
    private ViewGroup dynamicContent;

    public SidebarBuilder sidebar(Sidebar sidebar){
        this.sidebar = sidebar;
        return this;
    }

    public SidebarBuilder contentView(ViewGroup layout){
        this.dynamicContent = layout;
        return this;
    }

    public View build(Context ctx){
        //create root container
        HorizontalContainer root = new HorizontalContainer(ctx);
        //add sidebar
        if (this.sidebar == null){ this.sidebar = new Sidebar(ctx); }
        root.addView(this.sidebar);
        //add new content
        if (this.dynamicContent == null){ this.dynamicContent = new LinearLayout(ctx); }
        root.addView(this.dynamicContent);
        //result
        return root;
    }

}
