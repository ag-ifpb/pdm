package ifpb.ag.sidebarprogramatically;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DrawableUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.TextView;

import ifpb.ag.sidebarprogramatically.sidebar.Sidebar;
import ifpb.ag.sidebarprogramatically.sidebar.SidebarAnimated;
import ifpb.ag.sidebarprogramatically.sidebar.SidebarBuilder;
import ifpb.ag.sidebarprogramatically.sidebar.VerticalContainer;


//public class MainActivity extends AppCompatActivity implements Button.OnClickListener{
public class MainActivity extends Activity implements Button.OnClickListener{
    private Sidebar sidebar;
    private Button menubtn;
    private boolean withMenu;

    private ViewGroup content(){
        //container root
        VerticalContainer dynCont = new VerticalContainer(this);
        //hello world
        TextView hw =  new TextView(this);
        hw.setText("Hello World");
        //open menu
        menubtn = new Button(this);
        menubtn.setOnClickListener(this);
        //root
        dynCont.addView(hw);
        dynCont.addView(menubtn);
        //
        return dynCont;
    }

    private void hideMenu(){
        menubtn.setText("Open Menu");
        sidebar.hide();
        withMenu = false;
    }

    private void showMenu(){
        menubtn.setText("Close Menu");
        sidebar.show();
        withMenu = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //create menu sidebar
        sidebar = new Sidebar(this);
        sidebar.addItem(getResources().getDrawable(R.drawable.user), "Usuários");
        sidebar.addItem(getResources().getDrawable(R.drawable.trash), "Lixeira");
        //dynamic content
        ViewGroup dynCont = content();
        //build content root
        SidebarBuilder builder = new SidebarBuilder();
        builder.sidebar(sidebar);
        builder.contentView(dynCont);
        //set in activity
        setContentView(builder.build(this));
        //hide menu
        hideMenu();
    }

    @Override
    public void onClick(View view) {
        //TODO: fazer controle de sumir (use: GONE/VISIBLE)
        //TODO: estilize sidebar
        if (withMenu){
            hideMenu();
        } else {
            showMenu();
        }
    }

}
