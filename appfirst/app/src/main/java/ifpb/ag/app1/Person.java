package ifpb.ag.app1;

public class Person {
    private int code;
    private String name;

    public Person(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
