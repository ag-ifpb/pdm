package ifpb.ag.app1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ifpb.ag.app.R;

public class NamesAdapter extends BaseAdapter {
    static List<Person> names = new ArrayList<>();

    static {
        names.add(new Person(1, "Paulo"));
        names.add(new Person(2, "Pedro"));
        names.add(new Person(3, "Maria"));
        names.add(new Person(5, "João"));
        names.add(new Person(6, "Francisco"));
        names.add(new Person(7, "Ana"));
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        return names.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //
        Context ctx = parent.getContext();
        LayoutInflater li = LayoutInflater.from(ctx);
        //
        Person p = names.get(position);
        View v = li.inflate(R.layout.fragment_list_item, null);
        ((TextView) v.findViewById(R.id.tvCode)).setText(String.valueOf(p.getCode()));
        ((TextView) v.findViewById(R.id.tvName)).setText(p.getName());
        //
        return v;
    }
}
