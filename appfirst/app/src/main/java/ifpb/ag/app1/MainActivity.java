package ifpb.ag.app1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import ifpb.ag.app.R;

public class MainActivity extends AppCompatActivity {
    int REQUEST_IMAGE_CAPTURE = 1;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        TextView tv = new TextView(this);
        tv.setText("Opa");
        //
        setContentView(R.layout.activity_main);//<---

        addContentView(tv, new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        //
//        String url = "http://ifpb.edu.br";
//        final Uri uri = Uri.parse(url);
//        Button btnURL = findViewById(R.id.btn1);
//        btnURL.setOnClickListener(new Button.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                MainActivity.this.startActivity(intent);
//            }
//        });
//        //
//        Button btnOther = findViewById(R.id.btn2);
//        btnOther.setOnClickListener(new Button.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, MyActivity.class);
//                intent.putExtra("PARAM_MAIN_0", "Ari Garcia");
//                MainActivity.this.startActivity(intent);
//            }
//        });
//        //
//        Button btnCamera = findViewById(R.id.btn3);
//        btnCamera.setOnClickListener(new Button.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                    MainActivity.this.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//                }
//            }
//        });
//        //
//        Button btnList = findViewById(R.id.btn4);
//        btnList.setOnClickListener(new Button.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                Intent listIntent = new Intent(MainActivity.this, ListActivity.class);
//                MainActivity.this.startActivity(listIntent);
//            }
//        });
//
//        imageView = findViewById(R.id.img);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    }
}
