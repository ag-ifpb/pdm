package ifpb.ag.app1;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import ifpb.ag.app.R;

public class ListActivity extends Activity {
    private ListView listView;
    private BaseAdapter namesAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        //adapter
        namesAdapter = new NamesAdapter();

        //retrieve list view
        listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(namesAdapter);
    }
}
