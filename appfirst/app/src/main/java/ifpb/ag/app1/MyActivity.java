package ifpb.ag.app1;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;

public class MyActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        //
        super.onCreate(savedInstanceState);
        //
        Intent quemMeChamou = getIntent();
        String text = quemMeChamou.getStringExtra("PARAM_MAIN_0");
        //
        LinearLayout root = new LinearLayout(this);
        root.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));
        //root.setBackgroundColor(Color.RED);
        root.setOrientation(LinearLayout.VERTICAL);
        setContentView(root);
        //
        Box box1 = new Box(this, "Label1");
        box1.setText(text);
        root.addView(box1);
        //
        Box box2 = new Box(this, "Label2");
        root.addView(box2);


    }
}
