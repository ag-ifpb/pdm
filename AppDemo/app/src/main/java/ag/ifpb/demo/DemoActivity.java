package ag.ifpb.demo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;

import ag.ifpb.demo.adapter.ListViewAdapter;


public class DemoActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        //

        ListView lv = findViewById(R.id.list_view);
        lv.setAdapter(new ListViewAdapter());

    }
}
