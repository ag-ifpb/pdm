package ag.ifpb.demo.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import ag.ifpb.demo.comp.ListItem;

public class ListViewAdapter extends BaseAdapter {

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //s
        Context ctx = parent.getContext();
        ListItem listItem = new ListItem(ctx);
        listItem.setCode("code-00000000");
        listItem.setName("name-fulano de tal");
        listItem.setPhoto("file:///storage/emulated/legacy/Pictures/pexels-photo-1133957.jpeg");
        //
        return listItem;
    }
}
