package ag.ifpb.demo.comp;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;

import ag.ifpb.demo.R;

public class ListItem extends LinearLayout {
    private ImageView ivPhoto;
    private TextView tvCode;
    private TextView tvName;

    public ListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        //
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout ll = (LinearLayout) li.inflate(R.layout.compview_list_item, null);
        ivPhoto = ll.findViewById(R.id.ivPhoto);
        tvCode = ll.findViewById(R.id.tvCode);
        tvName = ll.findViewById(R.id.tvName);
        addView(ll);
    }

    public ListItem(Context context) {
        this(context, null);
    }

    public void setCode(String code){
        tvCode.setText(code);
    }

    public void setName(String name){
        tvName.setText(name);
    }

    public void setPhoto(String photoUrl){
        //file:///data/data/.../image.png
        Uri uri = Uri.parse(photoUrl);
        ivPhoto.setImageURI(uri);
        ivPhoto.invalidate();
    }

}
