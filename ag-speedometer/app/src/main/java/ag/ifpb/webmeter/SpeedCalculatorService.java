package ag.ifpb.webmeter;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;


public class SpeedCalculatorService extends IntentService {

    public SpeedCalculatorService() {
        super("SpeedCalculatorService");
    }

    private int calculator(int size, long time) {
        //dividindo tamanho (Mb)
        return Math.round(size * 1024/time);//in kbps
    }

    //TODO: tarefa 1 - fazer o download de verdade
    private long download(int size){//em ms
        try {
            //simulando o download de 1M * size
            long fakeTime = Math.round(size + (Math.random()*1000));
            //delay of the download
            Thread.sleep(fakeTime*10);
            //result
            return fakeTime;
        } catch (InterruptedException e){}
        //result
        return 0;
    }

    private void notifyUI(int speed){
        ChangedSpeedEventReceiver.sendBroadcast(getApplicationContext(), speed);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //
        SignalEventReceiver.sendBroadcast(getApplicationContext(), SignalEventReceiver.SIGNAL_START);
        //simulando o download de 1M
        long t_1 = download(1);
        int s_1 = calculator(1, t_1);
        notifyUI(s_1);
        //TODO: verificar se foi solicitado parada
        //simulando o download de 5M
        long t_5 = download(5);
        int s_5 = calculator(5, t_5);
        notifyUI(s_5);
        //TODO: verificar se foi solicitado parada
        //simulando o download de 10M
        long t_10 = download(10);
        int s_10 = calculator(10, t_10);
        notifyUI(s_10);
        //
        SignalEventReceiver.sendBroadcast(getApplicationContext(), SignalEventReceiver.SIGNAL_STOP);
    }


    /**
     * Inicializa o serviço de cálculo de download
     *
     * @see IntentService
     */
    public static void start(Context context) {
        Intent intent = new Intent(context, SpeedCalculatorService.class);
        context.startService(intent);
    }
}
