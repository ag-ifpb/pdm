package br.edu.ifpb.buttontest;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;

public class FirstService extends Service {

    private String getResponse(){
        String response = "";
        try {
            URL url = new URL("http://ag-paudabandeira-mobile.appspot.com/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            response = conn.getResponseMessage();
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("AGDebug", response);
        return response;
    }

    private void delaoy() {
//        try {
//            Thread.sleep(10000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        double i = 10e9;//10*10^3
        while((i-- > 0));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("AGDebug", "Serviço Iniciado...");
        //
        final Message m = Message.obtain();
        //
        Thread thread = new Thread(){
            @Override
            public void run() {
                //não é o procedimento correto
                //SecondActivity.btn2.setText(getResponse());
                //procedimento correto
                m.obj =  getResponse();
                SecondActivity.myHandler.sendMessage(m);
            }
        };
        thread.start();
        //
        delaoy();
        //
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
