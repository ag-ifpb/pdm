package br.edu.ifpb.buttontest;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        Button btn = (Button) findViewById(R.id.btn1);
        btn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(
                        MainActivity.this, SecondActivity.class
                ));
            }
        });
        Log.d("AGDebug", "Criando MainActivity: " + toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("AGDebug", "Iniciando MainActivity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("AGDebug", "Parando MainActivity");
    }

    //O método deve ser públic e na sua assinatura deve ter um
    //único parâmtro que descende de View
    public void doClick(View v){
        Log.d("AGDEBUG", v.toString());
    }
}
