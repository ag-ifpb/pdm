package br.edu.ifpb.buttontest;

import android.os.Handler;
import android.os.Message;
import android.widget.Button;

public class MyHandler extends Handler {
    private final Button btn;

    public MyHandler(Button b){
        this.btn = b;
    }

    @Override
    public void handleMessage(Message msg) {
        String text = (String) msg.obj;
        btn.setText(text);
    }
}
