package br.edu.ifpb.buttontest;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {
    static Button btn2;
    static MyHandler myHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_second);
        //
        btn2 = (Button) findViewById(R.id.btn2);
        Button btn3 = (Button) findViewById(R.id.btn3);
        //
        btn2.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                //Forma errada de chamar a activity anterior
                //SecondActivity.this.startActivity(new Intent(
                //        SecondActivity.this, MainActivity.class
                //));
                SecondActivity.this.finish();
            }
        });
        //
        btn3.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                SecondActivity.this.startService(new Intent(
                   SecondActivity.this, FirstService.class
                ));
            }
        });

        myHandler = new MyHandler(btn2);

    }


}
