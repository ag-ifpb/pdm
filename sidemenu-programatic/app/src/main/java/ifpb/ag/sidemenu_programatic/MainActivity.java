package ifpb.ag.sidemenu_programatic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


//public class MainActivity extends AppCompatActivity {
public class MainActivity extends Activity implements NavigationView.OnNavigationItemSelectedListener {

    private ViewGroup header(){
        //
        TextView tv = new TextView(this);
        tv.setText("Cabeçalho");
        //
        LinearLayout l = new LinearLayout(this);
        l.addView(tv);
        //
        return l;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //criando o layout e vinculando ao activity
        DrawerLayout drawer = new DrawerLayout(this);
        setContentView(drawer);
        //criando o navbar
        AppBarLayout bar = new AppBarLayout(this);
        Toolbar toolbar = new Toolbar(this);
        toolbar.setBackgroundColor(Color.RED);
        //setSupportActionBar(toolbar); descomente se tiver usando AppCompatActivity
        bar.addView(toolbar);
        drawer.addView(bar, 0, new DrawerLayout.LayoutParams(
                DrawerLayout.LayoutParams.MATCH_PARENT,//comprimento
                DrawerLayout.LayoutParams.WRAP_CONTENT//altura
        ));
        //
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);//para sincronizar ao fechar
        toggle.syncState();
        //
        //criar os paramentros do layout para definir a gravidade
        DrawerLayout.LayoutParams params = new DrawerLayout.LayoutParams(
                DrawerLayout.LayoutParams.WRAP_CONTENT,//comprimento
                DrawerLayout.LayoutParams.MATCH_PARENT//altura
        );
        params.gravity = Gravity.LEFT;//informar de onde sai a bandeija
        //adicionar ao layout
        NavigationView navigationView = new NavigationView(this);
        drawer.addView(navigationView, 1, params);
        navigationView.getMenu().add(0, 0, 0, "menu #1").setIcon(R.drawable.ic_profile);
        navigationView.getMenu().add(0, 1, 1, "menu #2").setIcon(R.drawable.ic_trash);
        //
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.addHeaderView(header());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if (menuItem.getItemId() == 0){
            Toast.makeText(this, "Selecionou o menu 1", Toast.LENGTH_LONG).show();
        } else if (menuItem.getItemId() == 1){
            Toast.makeText(this, "Selecionou o menu 2", Toast.LENGTH_LONG).show();
        }
        return false;
    }
}
