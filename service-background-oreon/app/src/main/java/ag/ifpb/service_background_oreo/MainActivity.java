package ag.ifpb.service_background_oreo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static android.content.Intent.FLAG_INCLUDE_STOPPED_PACKAGES;

//adb -s 192.168.56.101:5555 shell dumpsys alarm
//adb -s 192.168.56.101:5555 shell am broadcast -a ag.ifpb.service_background_oreo.ALARM -n ag.ifpb.service_background_oreo/.AlarmBroadcastReceiver
//adb -s 192.168.56.101:5555 shell am broadcast -a ag.ifpb.service_background_oreo.ALARM -n ag.ifpb.service_background_oreo/ag.ifpb.service_background_oreo.AlarmBroadcastReceiver
//ComponentInfo{ag.ifpb.service_background_oreo/ag.ifpb.service_background_oreo.AlarmBroadcastReceiver}
public class MainActivity extends AppCompatActivity {

    private void regiserAlarm(){
        //
        //Intent broadcastIntent = new Intent(this, AlarmBroadcastReceiver.class);
        //broadcastIntent.setAction("ag.ifpb.service_background_oreo.ALARM");
        //broadcastIntent.setFlags(FLAG_INCLUDE_STOPPED_PACKAGES);
        //Intent broadcastIntent = new Intent(getApplicationContext(), AlarmBroadcastReceiver.class);
        //
        ComponentName cname = new ComponentName(this, AlarmBroadcastReceiver.class);
        Intent broadcastIntent = new Intent();
        broadcastIntent.setComponent(cname);
        broadcastIntent.setPackage(getPackageName());
        broadcastIntent.setAction("ag.ifpb.service_background_oreo.ALARM");
        broadcastIntent.setFlags(FLAG_INCLUDE_STOPPED_PACKAGES);
        //
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, broadcastIntent, 0);
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        long timeInterval = 10 * 1000;
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), timeInterval, pi);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        final TextView tvCount = findViewById(R.id.txCount);
        Counter.init(new Observer() {
            @Override
            public void onUpdate(final int count) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvCount.setText(String.valueOf(count));
                    }
                });
            }
        });
        //
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                //Intent aIntent = new Intent(MainActivity.this, AlarmIntentService.class);
                //MainActivity.this.startService(aIntent);
                regiserAlarm();
                Log.d("AGDEBUG", "registerAlarm");
                finish();
            }
        });

        ComponentName cname = new ComponentName(this, AlarmBroadcastReceiver.class);
        Log.d("AGDEBUG", cname.toString());
    }
}
