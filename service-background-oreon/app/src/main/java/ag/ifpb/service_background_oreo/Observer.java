package ag.ifpb.service_background_oreo;


public interface Observer {
    void onUpdate(int count);
}
