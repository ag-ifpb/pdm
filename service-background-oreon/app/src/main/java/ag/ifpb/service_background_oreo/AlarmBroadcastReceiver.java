package ag.ifpb.service_background_oreo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent aIntent = new Intent(context, AlarmIntentService.class);
        context.startService(aIntent);
    }
}
