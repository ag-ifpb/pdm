package ag.ifpb.service_background_oreo;

class Counter {
    private static int count = 0;
    private static Counter ourInstance = null;

    static Counter getInstance() {
        return ourInstance;
    }

    static Counter init(Observer obs) {
        ourInstance = new Counter(obs);
        return ourInstance;
    }

    private final Observer obs;

    private Counter(Observer obs) {
        this.obs = obs;
    }

    public void increaseCount(){
        count++;
        if (obs != null){
            obs.onUpdate(count);
        }

    }

}
