package ag.ifpb.service_background_oreo;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class AlarmIntentService extends IntentService {

    public AlarmIntentService() {
        super("AlarmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Counter c = Counter.getInstance();
        c.increaseCount();
        Log.d("AGDEBUG", "run service");

    }

}
